#!/usr/bin/env python3
# coding=utf-8
#
# Kml2Svg Inkscape extension
#
# Copyright (C) 2021 Franklin JARRIER, https://gitlab.com/franklin204/inkscape-extension
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Input a KML file
"""

import os
import re
import xml.etree.ElementTree as ET
import inkex
import random
import tempfile
import zipfile
from base64 import encodebytes
import mimetypes
from kml2svg_proj import Kml2SvgProj


class KmlInput(inkex.InputExtension, Kml2SvgProj):
    cpt_id = 0
    min_x = +99999
    max_x = -99999
    min_y = +99999
    max_y = -99999
    min_lon_kml = +180
    max_lon_kml = -180
    min_lat_kml = +90
    max_lat_kml = -90
    all_style_map = {}
    all_styles = {}
    all_schemas = {}
    tmp_dir_name = None

    def trace_debug(self, text):
        if self.options.debug_output:
            self.debug(text)

    def set_name_and_description(self, kml_elt, svg_elt):
        self.trace_debug(f"set_name_and_description {kml_elt}")
        name_tag = kml_elt.find('.//{*}name')
        description_tag = kml_elt.find('.//{*}description')
        extended_tag = kml_elt.find('.//{*}ExtendedData')
        name = name_tag.text if name_tag is not None else None
        description = description_tag.text if description_tag is not None else None
        if name is not None:
            title_elt = svg_elt.add(inkex.Title())
            title_elt.text = name
        if description is not None or extended_tag is not None:
            desc_elt = svg_elt.add(inkex.Desc())
            description_text = f"{description}" if description is not None else ""

            # TODO : Make json ?
            if extended_tag is not None:
                for data in extended_tag.findall(".//{*}Data"):
                    if data.find('.//{*}value') is not None:
                        description_text += f"{data.get('name')}:{data.find('.//{*}value').text}\n"
            self.trace_debug(f"Add description {description_text}")
            desc_elt.text = description_text

    def read_coordinates(self, coordinate, reverse):
        text_coordinates = coordinate.text.strip()
        result_coord = []
        if len(text_coordinates) == 0:
            return result_coord

        if coordinate.get("cs") and coordinate.get("ts") and coordinate.get("decimal"):
            cs = coordinate.get("cs")
            ts = coordinate.get("ts")
            if ts == " ":
                coordinates = re.compile('[ \t\n\r\f\v]+').split(text_coordinates)
            else:
                coordinates = text_coordinates.split(ts)

            self.trace_debug(f"coordinates.2={coordinates}  reverse={reverse}")
            coordinates = reversed(coordinates) if reverse else coordinates

            for coordinate in coordinates:
                values = coordinate.split(cs)
                val0 = values[0].strip()
                val1 = values[1].strip()
                val2 = values[2].strip() if len(values) > 2 else 0
                x_y = self.convert_coord(val1, val0)
                result_coord.append(
                    {'x': x_y["x"], 'y': x_y["y"], 'alt': float(val2), 'lon': float(val1), 'lat': float(val0)})
        else:
            self.trace_debug(f"coordinates.4={text_coordinates}")
            # stretch coordinates
            re_replacer = re.compile("([ ]*[,][ ]*)", re.IGNORECASE)
            text_coordinates = re_replacer.sub(',', text_coordinates)

            re_replacer = re.compile("(\r\n)|(\r)|(\n)", re.IGNORECASE)
            text_coordinates = re_replacer.sub(',', text_coordinates)
            text_coordinates = text_coordinates.strip()

            number_of_data_per_coord = len(text_coordinates.split(" ")[0].split(","))

            # limit to 2 or 3 just in case...
            if number_of_data_per_coord > 3:
                number_of_data_per_coord = 3
            coordinates = re.compile('[^-0-9e.]+').split(text_coordinates)
            self.trace_debug(f"coordinates.8={coordinates}  number_of_data_per_coord={number_of_data_per_coord}")
            size = len(coordinates)
            if not reverse:
                for j in range(0, size, number_of_data_per_coord):
                    if number_of_data_per_coord > 2:
                        alt = float(coordinates[j + 2].strip())
                    else:
                        alt = 0
                    lon = float(coordinates[j].strip())
                    lat = float(coordinates[j + 1].strip())
                    x_y = self.convert_coord(lat, lon)
                    result_coord.append({'x': x_y["x"], 'y': x_y["y"], 'alt': alt, 'lat': lat, 'lon': lon})
            else:
                for j in range(size - 1, 0, -number_of_data_per_coord):
                    if number_of_data_per_coord > 2:
                        lat = float(coordinates[j - 1].strip())
                        lon = float(coordinates[j - 2].strip())
                        alt = float(coordinates[j].strip())
                    else:
                        lat = float(coordinates[j].strip())
                        lon = float(coordinates[j - 1].strip())
                        alt = 0
                    x_y = self.convert_coord(lat, lon)
                    result_coord.append({'x': x_y["x"], 'y': x_y["y"], 'alt': alt, 'lon': lon, 'lat': lat})

        self.trace_debug(f"read_coordinates={result_coord}")
        return result_coord

    def extract_linear_ring(self, layer, name, line, style, name_tag, description_tag):
        return self.extract_line(layer, name, line, style, name_tag, description_tag)

    def extract_line(self, layer, name, line, style, name_tag, description_tag):
        if not self.options.user_extract_lines:
            return
        # remove filling from style for a line
        style['fill'] = 'none'
        coordinates_tag = line.find('{*}coordinates')
        self.trace_debug(f"extract_line {coordinates_tag}")
        if coordinates_tag is not None:
            self.trace_debug(f"extract_line with coordinates={coordinates_tag.text} style={style}")
            coordinates = self.read_coordinates(coordinates_tag, False)
            line = layer.add(inkex.PathElement(id=name))
            path = []
            for coord in coordinates:
                self.trace_debug(f"coord=={coord}")
                path.append(['M' if len(path) == 0 else 'L', [coord["x"], coord["y"]]])
                self.trace_debug(f"path.1=={path}")
            line.path = path
            # line.set('d', str(inkex.Path(path)))
            self.trace_debug(f"path={path}")
            line.style = style
            self.convert_placemark_description_to_text(layer, line, name_tag, description_tag)
            return line

    def extract_tour(self, layer, tour):
        if not self.options.user_extract_tours:
            return None

        coordinates = []
        path = []
        name_tag = tour.find('.//{*}name')
        name = f"{name_tag.text if name_tag is not None else 'Tour'}#{self.cpt_id}"
        line = layer.add(inkex.PathElement(id=name))

        # tours are Cameras or FlyTos elements
        for fly_to in tour.findall('.//{*}FlyTo'):
            latitude = float(fly_to.find('.//{*}longitude').text)
            longitude = float(fly_to.find('.//{*}latitude').text)
            coordinates.append(self.convert_coord(latitude, longitude))

        for camera in tour.findall('.//{*}Camera'):
            latitude = float(camera.find('.//{*}longitude').text)
            longitude = float(camera.find('.//{*}latitude').text)
            coordinates.append(self.convert_coord(latitude, longitude))

        self.trace_debug(f"extract Tour coordinates={coordinates}")
        for coord in coordinates:
            self.trace_debug(f"coord=={coord}")
            path.append(['M' if len(path) == 0 else 'L', [coord["x"], coord["y"]]])
            self.trace_debug(f"path.1=={path}")
        line.path = path
        # line.set('d', str(inkex.Path(path)))
        self.trace_debug(f"path={path}")
        line.style = self.get_default_style(False)
        self.set_name_and_description(tour, line)
        return line

    def extract_polygon_linear_ring(self, linear_ring, reverse):
        path = []
        coordinates_tag = linear_ring.find('{*}coordinates')
        self.trace_debug(f"extract_polygon_linear_ring with coordinates={coordinates_tag.text}")
        coordinates = self.read_coordinates(coordinates_tag, reverse)

        for coord in coordinates:
            self.trace_debug(f"coord=={coord}")
            path.append(['M' if len(path) == 0 else 'L', [coord["x"], coord["y"]]])
            self.trace_debug(f"path.1=={path}")

        return path

    def extract_polygon_boundary(self, boundary, reverse):
        path = []
        for linear_ring in boundary.findall('{*}LinearRing'):
            # do recursively
            if len(path) > 0:
                path.append(['z', []])
            path.extend(self.extract_polygon_linear_ring(linear_ring, reverse))

        return path

    def extract_polygon(self, layer, name, polygon, style, name_tag, description_tag):
        if not self.options.user_extract_polygons:
            return
        extrude = False
        extrude_tags = polygon.findall('{*}extrude')
        if len(extrude_tags) > 0:
            extrude = True if (float(extrude_tags[0].text) == 1.0) else False

        self.trace_debug(f"extract_polygon {name}")
        line = layer.add(inkex.PathElement(id=name))
        path = []

        for outer_boundary_is in polygon.findall('{*}outerBoundaryIs'):
            self.trace_debug(f"extract_polygon_boundary outer_boundary_is")
            path.extend(self.extract_polygon_boundary(outer_boundary_is, False))

        for inner_boundary_is in polygon.findall('{*}innerBoundaryIs'):
            self.trace_debug(f"extract_polygon_boundary inner_boundary_is")
            path.append(['z', []])
            path.extend(self.extract_polygon_boundary(inner_boundary_is, extrude))

        path.append(['z', []])
        self.trace_debug(f"path=={path}")

        line.path = path
        line.style = style
        
        self.convert_placemark_description_to_text(layer, line, name_tag, description_tag)
        return line

    def extract_point(self, layer, placemark, name, point):
        if not self.options.user_extract_points:
            return

        coordinates_tag = point.find('{*}coordinates')
        self.trace_debug(f"extract_point with coordinates={coordinates_tag.text}")
        coordinates = self.read_coordinates(coordinates_tag, False)

        if len(coordinates) > 0:
            coord = coordinates[0]
            label = re.sub(r'(.*)#([0-9]*)', r'\1', name, 1)
            text = self.add_text_label(coord, layer, label, label, 'start')
            self.set_name_and_description(placemark, text)
            return text

    def add_text_label(self, coord, layer, text_name, text_content, t_anchor):
        text = layer.add(inkex.TextElement(name=text_name, x=str(coord["x"]), y=str(coord["y"])))
        text.style = {'text-align': 'start',
                      'vertical-align': 'bottom',
                      'text-anchor': t_anchor,
                      'font-family': str(self.options.user_font),
                      'font-size': str(self.options.user_font_size) + 'px',
                      'fill-opacity': '1.0',
                      'stroke': 'none',
                      'font-weight': 'normal',
                      'font-style': 'normal',
                      'fill': '#000000'}
        text.text = text_content
        text.label = text_name
        return text

    def extract_geometry(self, layer, placemark, name, geometry, style, name_tag, description_tag):
        if not self.options.user_extract_multigeometries:
            return

        self.trace_debug(f"extract_geometry {name}")
        result = []

        for point in geometry.findall('{*}Point'):
            self.extract_point(layer, placemark, name, point)
        for line in geometry.findall('{*}LineString'):
            result.append(self.extract_line(layer, name, line, style, name_tag, description_tag))
        for line in geometry.findall('{*}LinearRing'):
            result.append(self.extract_linear_ring(layer, name, line, style, name_tag, description_tag))
        for polygon in geometry.findall('{*}Polygon'):
            result.append(self.extract_polygon(layer, name, polygon, style, name_tag, description_tag))
        for multi_geometry in geometry.findall('{*}MultiGeometry'):
            self.extract_geometry(layer, placemark, name, multi_geometry, style, name_tag, description_tag)
        for geometry2 in geometry.findall('{*}GeometryCollection'):
            self.extract_geometry(layer, placemark, name, geometry2, style, name_tag, description_tag)

        for r in result:
            self.set_name_and_description(placemark, r)

    def extract_ground_overlay(self, layer, overlay):
        if not self.options.user_extract_images:
            return
        href = ""

        if overlay.find('./{*}Icon/{*}href') is not None:
            href = overlay.find('./{*}Icon/{*}href').text

        # Get coordinates from LatLonBox
        for lat_lon_box in overlay.findall('.//{*}LatLonBox'):
            north = float(lat_lon_box.find('{*}north').text)
            south = float(lat_lon_box.find('{*}south').text)
            east = float(lat_lon_box.find('{*}east').text)
            west = float(lat_lon_box.find('{*}west').text)

            x_y = self.convert_coord(north, west)
            x_y2 = self.convert_coord(south, east)

            if x_y2["y"] < x_y["y"]:
                temp = x_y2["y"]
                x_y2["y"] = x_y["y"]
                x_y["y"] = temp

            height = abs(x_y["y"] - x_y2["y"])
            width = abs(x_y["x"] - x_y2["x"])
            cx = x_y["x"] + width / 2
            cy = x_y["y"] + height / 2

            image = layer.add(inkex.Image(x=str(x_y["x"]),
                                          y=str(x_y["y"]),
                                          height=str(height),
                                          width=str(width)
                                          ))
            if lat_lon_box.find('{*}rotation') is not None:
                rotation = -float(lat_lon_box.find('{*}rotation').text)
                image.set("transform", f"rotate({rotation}, {cx}, {cy})")
            # embed Image ?
            if self.tmp_dir_name and os.path.isfile(os.path.join(self.tmp_dir_name, href)):
                with open(os.path.join(self.tmp_dir_name, href), "rb") as image_data:
                    mimetype = mimetypes.MimeTypes().guess_type(os.path.join(self.tmp_dir_name, href))
                    if len(mimetype) > 0:
                        image.set('xlink:href', 'data:{};base64,{}'.format(mimetype[0],
                                                                           encodebytes(image_data.read()).decode(
                                                                               'ascii')))
            else:
                image.set("xlink:href", href)

            self.set_name_and_description(overlay, image)

    def is_add_extra_label(self, name_tag, description_tag):
        return (self.options.user_convert_placemark_name_to_text 
                and name_tag is not None 
                and name_tag.text is not None
            or self.options.user_convert_placemark_description_to_text 
                and description_tag is not None 
                and description_tag.text is not None)

    def extract_placemark(self, layer, placemark):
        name_tag = placemark.find('.//{*}name')
        description_tag = placemark.find('.//{*}description')
        self.cpt_id += 1

        name = f"{name_tag.text if name_tag is not None else 'Placemark'}#{self.cpt_id}"
        target_element = layer

        isElementWithExtraLabel = len(placemark.findall('{*}Polygon')) > 0 or len( placemark.findall('{*}MultiGeometry')) > 0 or len(placemark.findall('{*}GeometryCollection')) > 0

        if self.is_add_extra_label(name_tag, description_tag) and isElementWithExtraLabel:
            target_element = inkex.Group.new(f"virtual")
            layer.add(target_element)
            
            if name_tag is not None:
                name_tag = name_tag.text
            if description_tag is not None:
                description_tag = description_tag.text

        style = self.get_style_for_placemark(placemark)
        self.trace_debug(f"extract_placemark name={name} style={style}")
        result = []

        for line in placemark.findall('{*}LineString'):
            result.append(self.extract_line(target_element, name, line, style, name_tag, description_tag))

        for polygon in placemark.findall('{*}Polygon'):
            result.append(self.extract_polygon(target_element, name, polygon, style, name_tag, description_tag))

        for geometry in placemark.findall('{*}MultiGeometry'):
            self.extract_geometry(target_element, placemark, name, geometry, style, name_tag, description_tag)

        for geometry in placemark.findall('{*}GeometryCollection'):
            self.extract_geometry(target_element, placemark, name, geometry, style, name_tag, description_tag)

        for point in placemark.findall('{*}Point'):
            self.extract_point(target_element, placemark, name, point)

        for r in result:
            self.set_name_and_description(placemark, r)

    def convert_placemark_description_to_text(self, layer, svg_element, name_tag, description_tag):
        # if requested convert name and description to labels
        if self.options.user_convert_placemark_name_to_text or self.options.user_convert_placemark_description_to_text:
            center_x =  svg_element.bounding_box().left + (svg_element.bounding_box().right - svg_element.bounding_box().left) / 2
            center_y =  svg_element.bounding_box().top + (svg_element.bounding_box().bottom - svg_element.bounding_box().top) / 2 + self.options.user_font_size / 2

            if self.options.user_convert_placemark_name_to_text and name_tag:
                self.add_text_label({"x": center_x,
                                     "y": center_y},
                                    layer,
                                    f"virtual-{name_tag}",
                                    name_tag,
                                    'middle')

            if self.options.user_convert_placemark_description_to_text and description_tag:
                self.add_text_label({"x": center_x,
                                     "y": center_y + self.options.user_font_size if (self.options.user_convert_placemark_name_to_text and name_tag) else 0},
                                    layer,
                                    f"virtual-{description_tag}",
                                    description_tag,
                                    'middle')

    def extract_folder_content(self, layer, folder):
        self.trace_debug("extract_folder_content")
        for child in folder:
            self.trace_debug(f"{child.tag}, {child.attrib}")
            self.extract_from_tag_name(layer, child)

    def extract_folder(self, layer, folder, visibility):
        self.trace_debug(f"extract_folder {folder} visibility={visibility}")
        if self.options.user_extract_folders:
            name = [n for n in folder if 'name' in n.tag]
            self.cpt_id += 1
            final_name = f"folder_{self.cpt_id}"
            if len(name) > 0:
                final_name = name[0].text
            self.trace_debug(f"create folder {folder} final_name={final_name} visibility={visibility} ")
            layer = layer.add(inkex.Layer.new(final_name))

            if not visibility:
                layer.style['display'] = 'none'

        if folder.find('.//{*}description') is not None:
            layer.set("description", folder.find('.//{*}description').text)
        self.extract_folder_content(layer, folder)

    def get_fill_color(self):
        if self.options.user_use_random_fill_color:
            color = self.options.user_default_color_fill.to_rgb()
            color.hue = int(random.random() * 255.0)
            color.saturation = int(random.random() * 255.0)
            color.lightness = int(random.random() * 255.0)
            return color
        else:
            return self.options.user_default_color_fill.to_rgb()

    def get_default_style(self, fill):
        self.trace_debug(f"self.options.user_default_color={self.options.user_default_color.to_rgba().to_floats()}")
        self.trace_debug(
            f"self.options.user_default_color_fill={self.options.user_default_color_fill.to_rgba().to_floats()}   ")
        style = inkex.Style({'stroke-width': 1})
        style.set_color(self.get_fill_color(), 'fill')
        style.set_color(self.options.user_default_color, 'stroke')
        if fill:
            style['fill-opacity'] = self.options.user_default_color_fill.to_rgba().to_floats()[3] * 255
        else:
            style['fill'] = 'none'

        return style

    def get_style_for_placemark(self, placemark):
        self.trace_debug("get_style_for_placemark")

        style_tag = placemark.find('{*}Style')
        style_url_tag = placemark.find('{*}styleUrl')
        filled_object = True if placemark.find('{*}Polygon') is not None else False
        style = None
        self.trace_debug(f"Get style style_tag={style_tag} and style_url_tag={style_url_tag}")
        if style_tag is not None:
            self.trace_debug("extract_style from style_tag")
            style = self.extract_style(style_tag)
        elif style_url_tag is not None:
            style_url_tag_text = style_url_tag.text.replace('#', '')
            self.trace_debug(f"extract_style from style_url_tag={style_url_tag_text}")
            if style_url_tag_text in self.all_style_map:
                self.trace_debug(f"in all_style_map")
                style_map = self.all_style_map[style_url_tag_text]
                self.trace_debug(f"style_map={style_map}")
                if style_map in self.all_styles:
                    style = self.all_styles[style_map]
            elif style_url_tag_text in self.all_styles:
                self.trace_debug(f"in all_styles")
                if style_url_tag_text in self.all_styles:
                    style = self.all_styles[style_url_tag_text]

        self.trace_debug(f"extract_style from style_url_tag return {style}")

        if style is None:
            style = self.get_default_style(filled_object)
        self.trace_debug(f"get_style_for_placemark ={style.to_str()}")
        return style

    def extract_style_map(self, style_map):
        self.trace_debug(f"extract_style_map style_map={style_map}  id={style_map.get('id')}")
        for pair in style_map.findall('{*}Pair'):
            key = pair.find('{*}key').text
            style_url = pair.find('{*}styleUrl').text.replace('#', '')
            self.trace_debug(f"extract_style_map key={key} style_url={style_url}")
            if key == "normal":
                self.all_style_map[style_map.get('id')] = style_url

    def extract_schemas(self, schema):
        self.all_schemas[schema.get("name")] = schema.get("parent")

    def get_id_no_namespace(self, element):
        if element.get('id') is not None:
            return element.get('id')
        # Arbitrary return the first attribute 'id' with a namespace 
        for attribute in element.attrib:
            if re.match(r'\{.*\}id', attribute):
                return element.get(attribute)

    def extract_style(self, style):
        style_id = self.get_id_no_namespace(style)
        self.trace_debug(f"extract_style id={style_id}   style={style}")
        
        if not style_id:
            return

        if re.match('.*CascadingStyle', style.tag):
            # Arbitrary find the first Style element (CascadingStyle is not a shema : http://developers.google.com/kml/schema/kml22gx.xsd)
            style = style.find(".//{*}Style")

        inkex_style = self.get_default_style(True)

        if style.find('{*}LineStyle') is not None:
            style_info = self.get_color_and_width(style.find('{*}LineStyle'))
            self.trace_debug(f"extract_style LineStyle = {style_info}")
            if style_info is not None:
                if 'opacity' in style_info:
                    inkex_style['stroke-opacity'] = style_info['opacity']
                if 'color' in style_info:
                    inkex_style['stroke'] = style_info['color']
                if 'width' in style_info:
                    inkex_style['stroke-width'] = style_info['width']

        if style.find('{*}PolyStyle') is not None:
            style_info = self.get_color_and_width(style.find('{*}PolyStyle'))
            self.trace_debug(f"extract_style PolyStyle = {style_info}")
            if style_info is not None:
                if 'fill' not in style_info or style_info['fill'] == 1.0:
                    if 'color' in style_info:
                        inkex_style['fill'] = style_info['color']
                    else:
                        inkex_style['fill'] = self.get_fill_color()
                else:
                    inkex_style['fill'] = "none"

                if 'outline' in style_info and style_info['outline'] != 1.0:
                    inkex_style['stroke'] = "none"

                if 'opacity' in style_info:
                    inkex_style['fill-opacity'] = style_info['opacity']
        else:
            inkex_style['fill'] = 'none'

        self.all_styles[style_id] = inkex_style

        self.trace_debug(f"extract_style {inkex_style}")
        return inkex_style

    def get_color_and_width(self, style_element):
        result = {}
        color = style_element.find('{*}color')
        if color is not None:
            color = color.text
            color = color.replace("0x", "")
            opacity = color[:2]
            final_color = f"#{color[6:]}{color[4:-2]}{color[2:-4]}"
            opacity = int(opacity, 16) / 255
            result['opacity'] = opacity
            result['color'] = final_color
        width = style_element.find('{*}width')
        if width is not None:
            result['width'] = width.text
        fill = style_element.find('{*}fill')
        if fill is not None:
            result['fill'] = float(fill.text)
        outline = style_element.find('{*}outline')
        if outline is not None:
            result['outline'] = float(outline.text)
        color_mode = style_element.find('{*}colorMode')
        if color_mode is not None and color_mode.text == "random":
            return None
        return result

    def extract_links(self, link):
        self.trace_debug("extract_links {link}")
        inkex.errormsg(f"NetworkLinks are not extracted : {link.find('.//{*}href').text}")

    def extract_from_tag_name(self, layer, element, is_root = False):
        self.trace_debug(f"extract_from_tag_name {element}")

        visibility_tag = element.find('{*}visibility')
        visibility = False if (visibility_tag is not None and visibility_tag.text == "0") else True
        if self.options.user_force_visibility:
            visibility = True
        self.trace_debug(f"extract_from_tag_name element={element}, visibility={visibility}")

        if not self.options.user_extract_hidden and not visibility:
            return

        element_type = re.sub(r'\{.*\}(.*)', r'\1', element.tag, 1)

        # map if schemas
        if element_type in self.all_schemas:
            element_type = self.all_schemas[element_type]

        if "Document" in element_type:
            if is_root:
                self.extract_folder_content(layer, element)
            else :
                self.extract_folder(layer, element, visibility)
        elif "Folder" in element_type:
            self.extract_folder(layer, element, visibility)
        elif "Placemark" in element_type:
            self.extract_placemark(layer, element)
        elif "GroundOverlay" in element_type:
            self.extract_ground_overlay(layer, element)
        elif "NetworkLink" in element_type:
            self.extract_links(element)
        elif "Tour" in element_type:
            self.extract_tour(layer, element)

    def add_arguments(self, pars):
        pars.add_argument("--mode")
        pars.add_argument("--tab")
        pars.add_argument("--user_extract_folders", default=True, type=inkex.Boolean)
        pars.add_argument("--user_extract_hidden", default=True, type=inkex.Boolean)
        pars.add_argument("--user_force_visibility", default=False, type=inkex.Boolean)
        pars.add_argument("--user_extract_points", default=True, type=inkex.Boolean)
        pars.add_argument("--user_extract_lines", default=True, type=inkex.Boolean)
        pars.add_argument("--user_extract_tours", default=True, type=inkex.Boolean)
        pars.add_argument("--user_extract_polygons", default=True, type=inkex.Boolean)
        pars.add_argument("--user_extract_multigeometries", default=True, type=inkex.Boolean)
        pars.add_argument("--user_extract_images", default=True, type=inkex.Boolean)
        pars.add_argument("--user_convert_placemark_name_to_text", default=False, type=inkex.Boolean)
        pars.add_argument("--user_convert_placemark_description_to_text", default=False, type=inkex.Boolean)
        pars.add_argument("--user_output_max_size", default=1200)
        pars.add_argument("--user_default_color", type=inkex.Color, default=inkex.Color(0))
        pars.add_argument("--user_default_color_fill", type=inkex.Color, default=inkex.Color(0))
        pars.add_argument("--user_use_random_fill_color", default=False, type=inkex.Boolean)
        pars.add_argument("--user_font_size", type=float, default=10)
        pars.add_argument("--user_font", default="Arial")
        pars.add_argument("--projection", default="Mercator")
        pars.add_argument("--debug_output", default=False, type=inkex.Boolean)
        pars.add_argument("--lat_s", default=0.0)
        pars.add_argument("--lat_0", default=0.0)
        pars.add_argument("--lat_1", default=0.0)
        pars.add_argument("--lat_2", default=0.0)
        pars.add_argument("--lon_0", default=0.0)
        pars.add_argument("--lon_auto", default=True, type=inkex.Boolean)
        pars.add_argument("--encoding", dest="input_encode", default="latin_1")

    def load(self, stream):
        return stream

    def update_min_max_box_with_coord(self, x, y, box):
        if x < box["min_x"]:
            box["min_x"] = x
        if y < box["min_y"]:
            box["min_y"] = y
        if x > box["max_x"]:
            box["max_x"] = x
        if y > box["max_y"]:
            box["max_y"] = y

    def update_lax_lon_box_with_coord(self, lat, lon, box):
        if lon > box["max_lon_kml"]:
            box["max_lon_kml"] = lon
        if lon < box["min_lon_kml"]:
            box["min_lon_kml"] = lon
        if lat > box["max_lat_kml"]:
            box["max_lat_kml"] = lat
        if lat < box["min_lat_kml"]:
            box["min_lat_kml"] = lat

    def compute_min_max_box_document(self, doc):
        x_y_box = {"min_x": +99999, "max_x": -99999, "min_y": +99999, "max_y": -99999}
        lat_long_box = {"min_lat_kml": +90, "max_lat_kml": -90, "min_lon_kml": +180, "max_lon_kml": -180}
        self.compute_min_max_box(doc, x_y_box, lat_long_box)
        self.min_x = x_y_box["min_x"]
        self.max_x = x_y_box["max_x"]
        self.min_y = x_y_box["min_y"]
        self.max_y = x_y_box["max_y"]
        self.min_lon_kml = lat_long_box["min_lon_kml"]
        self.max_lon_kml = lat_long_box["max_lon_kml"]
        self.min_lat_kml = lat_long_box["min_lat_kml"]
        self.max_lat_kml = lat_long_box["max_lat_kml"]

    def compute_min_max_box(self, kml_elt, x_y_box, lat_long_box):
        # Get coordinates from KML element
        for coordinates_tag in kml_elt.findall('.//{*}coordinates'):
            self.trace_debug(f"coordinates_tag={coordinates_tag}")
            coordinates = self.read_coordinates(coordinates_tag, False)
            for coord in coordinates:
                self.trace_debug(f"coord={coord}")
                self.update_lax_lon_box_with_coord(coord["lat"], coord["lon"], lat_long_box)
                self.update_min_max_box_with_coord(coord["x"], coord["y"], x_y_box)

        # Get coordinates from LatLonBox
        for x_y_tag in kml_elt.findall('.//{*}LatLonBox'):
            north = float(x_y_tag.find('{*}north').text)
            south = float(x_y_tag.find('{*}south').text)
            east = float(x_y_tag.find('{*}east').text)
            west = float(x_y_tag.find('{*}west').text)

            x_y = self.convert_coord(south, west)
            self.update_lax_lon_box_with_coord(south, west, lat_long_box)
            self.update_min_max_box_with_coord(x_y["x"], x_y["y"], x_y_box)

            x_y = self.convert_coord(north, east)
            self.update_lax_lon_box_with_coord(north, east, lat_long_box)
            self.update_min_max_box_with_coord(x_y["x"], x_y["y"], x_y_box)

        # Get coordinates from Camera
        for x_y_tag in kml_elt.findall('.//{*}Camera'):
            longitude = float(x_y_tag.find('{*}longitude').text)
            latitude = float(x_y_tag.find('{*}latitude').text)

            x_y = self.convert_coord(latitude, longitude)
            self.update_lax_lon_box_with_coord(latitude, longitude, lat_long_box)
            self.update_min_max_box_with_coord(x_y["x"], x_y["y"], x_y_box)


        # Get coordinates from FlyTo
        for fly_to in kml_elt.findall('.//{*}FlyTo'):
            latitude = float(fly_to.find('.//{*}longitude').text)
            longitude = float(fly_to.find('.//{*}latitude').text)

            x_y = self.convert_coord(latitude, longitude)
            self.update_lax_lon_box_with_coord(latitude, longitude, lat_long_box)
            self.update_min_max_box_with_coord(x_y["x"], x_y["y"], x_y_box)

        self.trace_debug(f'x_y_bbox={x_y_box["min_x"]}/{x_y_box["min_y"]}/{x_y_box["max_x"]}/{x_y_box["max_y"]}')

    def need_to_auto_compute_lon0(self):
        return self.options.mode == "expert_mode" \
               and self.options.lon_auto \
               and (self.options.projection == "Polyconic"
                    or self.options.projection == "Van der Grinten I"
                    or self.options.projection == "Lambert Azimuthal Equal-Area"
                    or self.options.projection == "Albers Equal-Area Conic"
                    or self.options.projection == "Conic Equidistant"
                    or self.options.projection == "Werner"
                    or self.options.projection == "Bonne"
                    or self.options.projection == "Lambert Conformal Conic"
                    or self.options.projection == "Cassini"
                    or self.options.projection == "Azimuthal Equidistant"
                    or self.options.projection == "Gnomonic")

    def configure_basic_mode(self):
        if self.options.mode == "basic_mode":
            self.options.user_extract_folders = True
            self.options.user_extract_hidden = True
            self.options.user_force_visibility = True
            self.options.user_extract_points = True
            self.options.user_extract_lines = True
            self.options.user_extract_tours = True
            self.options.user_extract_polygons = True
            self.options.user_extract_multigeometries = True
            self.options.user_extract_images = True
            self.options.user_output_max_size = 1200
            self.options.user_default_color = inkex.Color("#000000")
            self.options.user_default_color_fill = inkex.Color("#000000").to_rgba(0.8)
            self.options.user_use_random_fill_color = False
            self.options.user_font_size = 10
            self.options.user_font = "Arial"
            self.options.projection = "Mercator"
            self.options.debug_output = False
            self.options.user_convert_placemark_name_to_text = False
            self.options.user_convert_placemark_description_to_text = False

    def process_kml_file(self, filename, doc):
        with open(filename, 'rb') as file:
            # remove all non UTF-8 characters from file for parsing
            data = file.read().decode('utf-8', 'ignore').encode("utf-8")

            parser = ET.XMLParser(encoding="utf-8")
            tree = ET.fromstring(data, parser=parser)
            root = tree  # .getroot()

            self.compute_min_max_box_document(root)

            if self.need_to_auto_compute_lon0():
                self.options.lon_0 = self.min_lon_kml + (self.max_lon_kml - self.min_lon_kml) / 2
                self.trace_debug(f"self.options.lon_0={self.options.lon_0}")
                inkex.errormsg(
                    f"\nlon_0={self.options.lon_0}.\n"
                    f"Keep this value if you plan to convert new file with the exact same projection !")
                # Need to recompute the min max box with new projection values
                # bad performances (need to split read_coordinates in 22 methods to read then convert)
                self.compute_min_max_box_document(root)

            if not doc:
                self.doc_width = 0
                self.doc_height = 0
                if self.min_y == self.max_y:
                    self.min_y -= 0.1
                    self.max_y += 0.1
                if self.min_x == self.max_x:
                    self.min_x -= 0.1
                    self.max_x += 0.1
                self.doc_width = self.max_x - self.min_x
                self.doc_height = self.max_y - self.min_y
                self.padding_x = self.min_x
                self.padding_y = self.min_y
                self.trace_debug(f"self.doc_width={self.doc_width}")
                self.trace_debug(f"self.doc_height={self.doc_height}")
                self.trace_debug(f"padding_x={self.padding_x}    padding_y={self.padding_y}")

                if self.doc_height > self.doc_width:
                    if self.doc_height == 0:
                        self.factor = 1
                    else:
                        self.factor = float(self.options.user_output_max_size) / self.doc_height
                    self.doc_height = float(self.options.user_output_max_size)
                    self.doc_width = self.factor * self.doc_width
                else:
                    if self.doc_width == 0:
                        self.factor = 1
                    else:
                        self.factor = float(self.options.user_output_max_size) / self.doc_width
                    self.doc_width = float(self.options.user_output_max_size)
                    self.doc_height = self.factor * self.doc_height

                self.trace_debug(f"self.factor={self.factor}")
                self.trace_debug(f"final   self.doc_width={self.doc_width}")
                self.trace_debug(f"final   self.doc_height={self.doc_height}")

                doc = self.get_template(width=self.doc_width, height=self.doc_height, unit='px')
                # Save values for SVG > KML
                # TODO : declare kml2svg prefix namespace ?
                doc.getroot().set("kml2svg_min_lat", self.min_lat_kml)
                doc.getroot().set("kml2svg_min_lon", self.min_lon_kml)
                doc.getroot().set("kml2svg_max_lat", self.max_lat_kml)
                doc.getroot().set("kml2svg_max_lon", self.max_lon_kml)
                doc.getroot().set("kml2svg_doc_width", self.doc_width)
                doc.getroot().set("kml2svg_doc_height", self.doc_height)
                doc.getroot().set("kml2svg_projection", self.options.projection)
                doc.getroot().set("kml2svg_lat_s", self.options.lat_s)
                doc.getroot().set("kml2svg_lat_0", self.options.lat_0)
                doc.getroot().set("kml2svg_lat_1", self.options.lat_1)
                doc.getroot().set("kml2svg_lat_2", self.options.lat_2)
                doc.getroot().set("kml2svg_lon_0", self.options.lon_0)
                doc.getroot().set("kml2svg_factor", self.factor)
                doc.getroot().set("kml2svg_padding_x", self.padding_x)
                doc.getroot().set("kml2svg_padding_y", self.padding_y)
                doc.getroot().set("id", os.path.basename(filename))
                doc.getroot().set("name", os.path.basename(filename))
                doc.getroot().set("title", os.path.basename(filename))

            for style in root.findall(".//{*}CascadingStyle"): # NB: CascadingStyle is not a shema : http://developers.google.com/kml/schema/kml22gx.xsd)
                self.trace_debug(f"extract_CascadingStyle for {style}")
                self.extract_style(style)
            for style in root.findall(".//{*}Style"):
                self.trace_debug(f"extract_style for {style}")
                self.extract_style(style)
            for schema in root.findall(".//{*}Schema"):
                self.trace_debug(f"extract_schemas for {schema}")
                self.extract_schemas(schema)
            for style_map in root.findall(".//{*}StyleMap"):
                self.trace_debug(f"extract_style_map for {style_map}")
                self.extract_style_map(style_map)

            self.trace_debug(f"all_styles={self.all_styles}")
            self.trace_debug(f"all_style_map={self.all_style_map}")

            self.trace_debug(f"create folder {os.path.basename(filename)}")
            root_layer = doc.getroot().add(inkex.Layer.new(os.path.basename(filename)))
            root_layer.add(inkex.Desc(filename))

            for child in root:
                self.trace_debug(f"{child.tag}, {child.attrib}")
                self.extract_from_tag_name(root_layer, child, True)
            return doc

    def effect(self):
        doc = None
        self.configure_basic_mode()

        if self.document.name.endswith(".kmz"):
            with tempfile.TemporaryDirectory() as tmp_dir:
                self.tmp_dir_name = tmp_dir
                with zipfile.ZipFile(self.document, 'r') as zip_ref:
                    zip_ref.extractall(tmp_dir)
                    for filename in os.listdir(tmp_dir):
                        if filename.endswith(".kml"):
                            doc = self.process_kml_file(os.path.join(tmp_dir, filename), doc)
        else:
            doc = self.process_kml_file(self.document.name, doc)

        # Set transparent background
        doc.getroot().namedview.set("inkscape:pagecheckerboard", "true")
        self.document = doc


if __name__ == '__main__':
    KmlInput().run()
