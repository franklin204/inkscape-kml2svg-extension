#!/usr/bin/env python3
# coding=utf-8
#
# Kml2Svg Inkscape extension
#
# Copyright (C) 2021 Franklin JARRIER, https://gitlab.com/franklin204/inkscape-extension
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
kml2svg projection
"""

import math
from math import cos, acos, sin, tan, asin, atan, atan2, sinh, radians, degrees, pow, log, sqrt
import inkex


# kml_elements = {'Placemark': extract_placemark,
#  'GroundOverlay': extract_overlay, 
#  'Folder': extract_folder, 
#  'Document': extract_folder, 
#  'Tour': extract_tour, 
#  'gx:Tour': extract_tour, 
#  'NetworkLink': extract_from_link
#  }


class Kml2SvgProj(object):
    padding_x = 0
    padding_y = 0
    factor = 1

    def convert_cylindrical_equal_area(self, lon, lat, lat_s):
        lat_s = radians(float(lat_s))
        x = lon * cos(lat_s)
        y = -(sin(lat) / cos(lat_s))

        return {"x": (x - self.padding_x) * self.factor,
                "y": (y - self.padding_y) * self.factor}

    def convert_cylindrical_equidistant(self, lon, lat, lon_0, lat_1):
        lat_1 = radians(float(lat_1))
        lon_0 = radians(float(lon_0))
        x = (lon - lon_0) * cos(lat_1)
        y = -lat

        return {"x": (x - self.padding_x) * self.factor,
                "y": (y - self.padding_y) * self.factor}

    def convert_bonne(self, lon, lat, lon_0, lat_1):
        lon_0 = radians(float(lon_0))
        lat_1 = radians(float(lat_1))
        if lat_1 < 0.0000000001:
            lat_1 = 0.0000000001

        p = self.cot(lat_1) + lat_1 - lat
        E = ((lon - lon_0) * cos(lat)) / p

        x = p * sin(E)
        y = -(self.cot(lat_1) - p * cos(E))

        return {"x": (x - self.padding_x) * self.factor,
                "y": (y - self.padding_y) * self.factor}

    def convert_coord(self, lat_deg, lon_deg):  # lat=lat, lon=lon
        lat = radians(float(lat_deg))
        lon = radians(float(lon_deg))

        # if lat == 0.0:
        #    lat = 0.00000000000000001
        # if lon == 0.0:
        #    lon = 0.00000000000000001
        if lat >= 90.0:
            lat = 89.99999999999999
        if lon >= 180.0:
            lon = 179.99999999999999
        if lat <= -90.0:
            lat = -89.99999999999999
        if lon <= -180.0:
            lon = -179.99999999999999

        self.trace_debug(f"convert_coord={self.options.projection}   lat={lat}   lon={lon}")

        if self.options.projection == "Mercator":
            # TODO -70 < lat < +70
            x = lon
            t = tan(math.pi / 4 + lat / 2)
            if t == 0: 
                y = 0
            else:
                y = -log(tan(math.pi / 4 + lat / 2), math.e)

        if self.options.projection == "Miller":
            x = lon
            y = -(1.25 * log(tan(math.pi / 4 + 0.8 * lat / 2), math.e))

        if self.options.projection == "Sinusoidal":
            x = lon * cos(lat)
            y = -lat

        if self.options.projection == "Van der Grinten I":
            if lat_deg == 0:
                lat = radians(0.00000001)

            if lon_deg == 0.0:
                lon = radians(0.00001)

            lon_0 = radians(float(self.options.lon_0))
            A = (1 / 2) * abs((math.pi / (lon - lon_0)) - ((lon - lon_0) / math.pi))
            theta = sin(abs((2 * lat) / math.pi))
            G = cos(theta) / (sin(theta) + cos(theta) - 1)
            P = G * (2 / sin(theta) - 1)
            Q = pow(A, 2) + G

            x = (math.pi * (A * (G - pow(P, 2)) + sqrt(
                pow(A, 2) * pow((G - pow(P, 2)), 2) - (pow(P, 2) + pow(A, 2)) * (
                        pow(G, 2) - pow(P, 2))))) / (pow(P, 2) + pow(A, 2))
            y = (math.pi * (P * Q - A * sqrt(
                (pow(A, 2) + 1) * (pow(P, 2) + (pow(A, 2))) - pow(Q, 2)))) \
                / (pow(P, 2) + pow(A, 2))

            if lat < 0:
                y = y
            else:
                y = -y

            if (lon - lon_0) < 0:
                x = - x

        if self.options.projection == "Polyconic":
            if lat_deg == 0:
                lat = radians(0.00000000000000001)

            if lon_deg == 0.0:
                lon = radians(0.00000000000000001)

            lat = radians(float(lat))
            lon = radians(float(lon))
            lat_0 = radians(float(self.options.lat_0))
            lon_0 = radians(float(self.options.lon_0))

            E = (lon - lon_0) * sin(lat)

            x = self.cot(lat) * sin(E)
            y = -((lat - lat_0) + self.cot(lat) * (1 - cos(E)))

        if self.options.projection == "Cylindrical Equidistant":
            return self.convert_cylindrical_equidistant(lon, lat, self.options.lon_0, self.options.lat_1)

        if self.options.projection == "Plate Carrée":
            return self.convert_cylindrical_equidistant(lon, lat, self.options.lon_0, 0)

        if self.options.projection == "Lambert Azimuthal Equal-Area":
            lat_1 = radians(float(self.options.lat_1))
            lon_0 = radians(float(self.options.lon_0))

            k = sqrt(2 / (1 + sin(lat_1) * sin(lat) + cos(lat_1)
                               * cos(lat) * cos(lon - lon_0)))

            x = k * cos(lat) * sin(lon - lon_0)
            y = -(k * (cos(lat_1) * sin(lat) - sin(lat_1) * cos(lat) * cos(lon - lon_0)))

        if self.options.projection == "Azimuthal Equidistant":
            if lat_deg == 0:
                lat = radians(0.001)

            if lon_deg == 0.0:
                lon = radians(0.001)

            lat = radians(float(lat))
            lon = radians(float(lon))
            lat_1 = radians(float(self.options.lat_1))
            lon_0 = radians(float(self.options.lon_0))

            c = math.acos((sin(lat_1) * sin(lat) + cos(lat_1) * cos(lat) * cos(lon - lon_0)))
            k = c / sin(c)

            x = k * cos(lat) * sin(lon - lon_0)
            y = -(k * (cos(lat_1) * sin(lat) - sin(lat_1) * cos(lat) * cos(lon - lon_0)))

        if self.options.projection == "Albers Equal-Area Conic":
            lat_0 = radians(float(self.options.lat_0))
            lon_0 = radians(float(self.options.lon_0))
            lat_1 = radians(float(self.options.lat_1))
            lat_2 = radians(float(self.options.lat_2))

            n = 1 / 2 * (sin(lat_1) + sin(lat_2))
            theta = n * (lon - lon_0)
            C = pow(cos(lat_1), 2) + 2 * n * sin(lat_1)
            p = sqrt(C - 2 * n * sin(lat)) / n
            p0 = sqrt(C - 2 * n * sin(lat_0)) / n

            x = p * sin(theta)
            y = -(p0 - p * cos(theta))

        if self.options.projection == "Conic Equidistant":
            lat_0 = radians(float(self.options.lat_0))
            lon_0 = radians(float(self.options.lon_0))
            lat_1 = radians(float(self.options.lat_1))
            lat_2 = radians(float(self.options.lat_2))

            n = (cos(lat_1) - cos(lat_2)) / (lat_2 - lat_1)
            theta = n * (lon - lon_0)
            G = cos(lat_1) / n + lat_1
            p = G - lat
            p0 = G - lat_0

            x = p * sin(theta)
            y = -(p0 - p * cos(theta))

        if self.options.projection == "Bonne":
            return self.convert_bonne(lon, lat, self.options.lon_0, self.options.lat_1)

        if self.options.projection == "Werner":
            return self.convert_bonne(lon, lat, self.options.lon_0, 90)

        if self.options.projection == "Lambert Conformal Conic":
            lon_0 = radians(float(self.options.lon_0))
            lat_0 = radians(float(self.options.lat_0))
            lat_1 = radians(float(self.options.lat_1))
            lat_2 = radians(float(self.options.lat_2))
            n = log(cos(lat_1) * self.sec(lat_2), math.e) / log(
                tan(1 / 4 * math.pi + 1 / 2 * lat_2) * self.cot(1 / 4 * math.pi + 1 / 2 * lat_1), math.e)
            F = (cos(lat_1) * pow(tan(1 / 4 * math.pi + 1 / 2 * lat_1), n)) / n
            p = F * pow(self.cot(1 / 4 * math.pi + 1 / 2 * lat), n)
            p0 = F * pow(self.cot(1 / 4 * math.pi + 1 / 2 * lat_0), n)

            x = p * sin(n * (lon - lon_0))
            y = -(p0 - p * cos(n * (lon - lon_0)))

        if self.options.projection == "Cylindrical Equal-Area":
            return self.convert_cylindrical_equal_area(lon, lat, self.options.lat_s)

        if self.options.projection == "Lambert":
            return self.convert_cylindrical_equal_area(lon, lat, "0")

        if self.options.projection == "Behrmann":
            return self.convert_cylindrical_equal_area(lon, lat, "30")

        if self.options.projection == "Tristan Edwards":
            return self.convert_cylindrical_equal_area(lon, lat, "37.383")

        if self.options.projection == "Peters":
            return self.convert_cylindrical_equal_area(lon, lat, "44.138")

        if self.options.projection == "Gall-Peters":
            return self.convert_cylindrical_equal_area(lon, lat, "45")

        if self.options.projection == "Balthasart":
            return self.convert_cylindrical_equal_area(lon, lat, "50")

        if self.options.projection == "Cassini":
            lon_0 = radians(float(self.options.lon_0))

            x = asin(cos(lat) * sin(lon - lon_0))
            y = atan2(-sin(lat), cos(lat) * cos(lon - lon_0))

        if self.options.projection == "Aitoff":
            x = (0.5 + 0.5 * sqrt(cos(lat))) * lon
            y = -(lat / (cos(0.5 * lat) * cos(1 / 6 * lon)))

        if self.options.projection == "Gnomonic":
            lon_0 = radians(float(self.options.lon_0))
            lat_1 = float(self.options.lat_1)
            if 0 == lat_1:
                lat_1 = 0.00000000001
            lat_1 = radians(lat_1)

            c = cos((sin(lat_1) * sin(lat) + cos(lat_1) * cos(lat) * cos(lon - lon_0)))
            x = (cos(lat) * sin(lon - lon_0)) / cos(c)
            y = - ((cos(lat_1) * sin(lat) - sin(lat_1)
                    * cos(lat) * cos(lon - lon_0)) / cos(c))

        return {"x": (x - self.padding_x) * self.factor,
                "y": (y - self.padding_y) * self.factor}

    def revert_coord(self, x: float, y: float):
        x = (x / float(self.svg.get("kml2svg_factor"))) + float(self.svg.get("kml2svg_padding_x"))
        y = (y / float(self.svg.get("kml2svg_factor"))) + float(self.svg.get("kml2svg_padding_y"))

        lon = 0
        lat = 0
        projection = self.svg.get('kml2svg_projection')
        # self.trace_debug(f"revert_coord={projection}   lat={lat}   lon={lon}")

        if projection == "Mercator":
            lon = x
            lat = -atan(sinh(y))
            lat = degrees(lat)
            lon = degrees(lon)

        return {"lat": lat,
                "lon": lon}

    def cot(self, a):
        return cos(a) / sin(a)

    def sec(self, a):
        return 1 / cos(a)
