#!/usr/bin/env python3
# coding=utf-8
#
# Kml2Svg Inkscape extension
#
# Copyright (C) 2023 Franklin JARRIER, https://gitlab.com/franklin204/inkscape-extension
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Kml2Svg extension setup.
"""

from setuptools import setup

setup(
           name='inkscape-kml2svg-extension',   
        version='0.0.7',
        description='KML/KMZ file Reader/writer',
        author='Franklin JARRIER',
        url='https://gitlab.com/franklin204/inkscape-kml2svg-extension',
        license='AGPLv3',
        classifiers=[
                'Environment :: Plugins',
                'Topic :: Import :: KML - KMZ',
                'Intended Audience :: Other Audience',   
                'Programming Language :: Python',
                'Programming Language :: Python :: 3.7',

        ],
        install_requires=[],
        setup_requires=["pytest-runner"]
        author_email='kml2svg@gmail.com',
)
