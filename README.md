# kml2svg inkscape extension

## About

This inkscape extension allow to import kml/kmz files directly in inkscape using various projections.

### Supported Projections
- Mercator (EPSG:3857)
- Miller (EPSG:54003)
- Cylindrical Equal-Area
  - Lambert
  - Behrmann
  - Tristan Edwards
  - Peters
  - Gall-Peters
  - Balthasart
-  Equidistant Cylindrical (WGS 84 / EPSG:4087)
  - Plate Carrée
-  Sinusoidal
-  Van der Grinten I
-  Polyconic
-  Albers Equal-Area Conic
-  Conic Equidistant
-  Bonne
  - Werner
-  Lambert Conformal Conic
-  Lambert Azimuthal Equal-Area
-  Azimuthal Equidistant
-  Cassini
-  Aitoff
-  Gnomonic

### Supported KML features:

- Folders are imported as layer.
- PlaceMarks are imported as text
- Tours are imported as multi lines.
- GroundOverlays are imported as images (limited support).
- Paths are imported as multi lines.
- Polygons are imported as filled multi lines.
- Volumes (Sketchup) are imported view from top only.
- Unselected (unticked) elements will be imported.
- Tours are imported as multi lines.
- Title and Description are imported in objects properties.

### Not supported:

- Screen overlays (banners) are not imported.
- NetworkLinks are not imported.

## Installation instruction:

- Copy files to your user extension folder :
  - unix: ~/.config/inkscape/extensions
  - windows: %APPDATA%\inkscape\extensions 
    
Inkscape Extension Manager support (Inkscape 1.1) under development

## Licence:

Copyright (C) 2021 Franklin JARRIER, https://gitlab.com/franklin204/inkscape-extension

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

See <https://www.gnu.org/licenses/>.

## About KML:

KML is an open standard officially named the OpenGIS® KML Encoding Standard (OGC KML). It is maintained by the Open Geospatial Consortium, Inc. (OGC). The complete specification for OGC KML can be found at http://www.opengeospatial.org/standards/kml/.
